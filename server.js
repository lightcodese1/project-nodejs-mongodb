const express = require('express');
const cors = require('cors');
const app = express();
const routerTutorial = require('./app/routes/tutorial.route');

const corsOptions = {
  origin: 'http://localhost:8081',
};

app.use(cors(corsOptions));

app.use(express.json());

app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use('/api/tutorials', routerTutorial);

app.get('/', (req, res) => {
  res.json({
    message: 'Welcome to bezkoder application.',
  });
});

const PORT = process.env.PORT || 8080;

const db = require('./app/models');

db.mongoose
  .connect(db.url, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Connected to the database!');
  })
  .catch((err) => {
    console.log('Cannot connect to the database!', err);
    process.exit();
  });

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
