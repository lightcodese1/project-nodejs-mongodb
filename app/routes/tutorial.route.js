const express = require('express');
const router = express.Router();
const tutorials = require('../controllers/tutorial.controller.js');

// Create a new Tutorial
router.post('/', tutorials.create);

// Retrieve all Tutorials
router.get('/', tutorials.findAll);

// Retrieve all published Tutorials
// router.get('/published', tutorials.findAllPublished);

// Retrieve a single Tutorial by id
router.get('/:id', tutorials.findOne);
// Update a Tutorial with id
router.put("/:id", tutorials.update);

module.exports = router;
